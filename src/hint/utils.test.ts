import { getHintNames } from './index';

describe('Test hint name generation', () => {
  let singleCharHints: string[] = [];
  let allHints: string[] = [];
  beforeAll(() => {
    singleCharHints = Array.from(getHintNames(12));
    allHints = Array.from(getHintNames(144));
  })
  test('returns single char hints, when hintCount < 12', () => {
    expect(singleCharHints[0]).toBe('q');
    expect(singleCharHints[5]).toBe('s');
    expect(singleCharHints[11]).toBe('v');
    expect(singleCharHints[12]).toBeUndefined();
  });

  test('returns double char hints starting with w, when hintCount < 24', () => {
    expect(allHints[0]).toBe('qq');
    expect(allHints[13]).toBe('ww');
    expect(allHints[18]).toBe('wd');
    expect(allHints[23]).toBe('wv');
  });
  test('returns double char hints starting with e, when hintCount > 24 but < 36', () => {
    expect(allHints[24]).toBe('eq');
    expect(allHints[27]).toBe('er');
    expect(allHints[35]).toBe('ev');
  });
  test('returns double char hints starting with f, when hintCount > 84 but < 96', () => {
    expect(allHints[84]).toBe('fq');
    expect(allHints[90]).toBe('fd');
    expect(allHints[95]).toBe('fv');
    expect(allHints[131]).toBe('cv');
    expect(allHints[143]).toBe('vv');
    expect(allHints[145]).toBeUndefined();
    expect(allHints).toHaveLength(144);
  });
});
