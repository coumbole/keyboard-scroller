import {
  NEW_SCROLL_TARGET,
  NEW_MOUSE_COORDS,
  ADD_HINT,
  REMOVE_HINT,
  ADD_HOST,
  START_HINTING,
  STOP_HINTING,
  RESET_HINTING,
  PRESSED_KEY,
  SHOW_HELPER,
  HIDE_HELPER,
  Action
} from './types';
import Hint from './hint';

export const newScrollTarget = (newTarget: Element): Action => {
  return {
    type: NEW_SCROLL_TARGET,
    target: newTarget
  };
};

export const newMouseCoords = (x: number, y: number): Action => {
  return {
    type: NEW_MOUSE_COORDS,
    x,
    y
  };
};

export const addHint = (hint: Hint): Action => {
  return {
    type: ADD_HINT,
    hint
  };
};

export const removeHint = (hint: Hint): Action => {
  return {
    type: REMOVE_HINT,
    hint
  };
};

export const addHost = (): Action => {
  return {
    type: ADD_HOST
  };
};


export const startHinting = (): Action => {
  return {
    type: START_HINTING
  };
};

export const stopHinting = (): Action => {
  return {
    type: STOP_HINTING
  };
};

export const resetHinting = (): Action => {
  return {
    type: RESET_HINTING
  };
};

export const keyPressed = (key: string): Action => {
  return {
    type: PRESSED_KEY,
    key,
    time: Date.now()
  };
};

export const showHelper = (): Action => {
  return {
    type: SHOW_HELPER
  };
};

export const hideHelper = (): Action => {
  return {
    type: HIDE_HELPER
  };
};
