import {
  NEW_SCROLL_TARGET,
  NEW_MOUSE_COORDS,
  ADD_HINT,
  REMOVE_HINT,
  START_HINTING,
  STOP_HINTING,
  RESET_HINTING,
  ADD_HOST,
  PRESSED_KEY,
  SHOW_HELPER,
  HIDE_HELPER,
  Action,
  State
} from './types';
import { resetHinting } from './actions';
import Hint from './hint';
import Helper from './helper';
import { detectHintSequence, hintMatchesSequence } from './hint';

const initialState: State = {
  currentTarget: null,
  x: 0,
  y: 0,
  hints: [],
  hintHost: null,
  isHintingActive: false,
  keySequence: [],
  lastKeyTime: 0,
  timeout: null,
  helper: null,
}

let seq: string[] = [];
let lastKeyTime = Date.now();

export const scrollReducer = (
  state = initialState,
  action: Action
): State => {
  switch (action.type) {
    case NEW_SCROLL_TARGET:
      return {
        ...state,
        currentTarget: action.target
      };
    case NEW_MOUSE_COORDS:
      return {
        ...state,
        x: action.x,
        y: action.y
      };
    case ADD_HINT:
      state.hintHost.appendChild(action.hint.container);
      return {
        ...state,
        hints: state.hints.concat(action.hint)
      };
    case REMOVE_HINT:
      return {
        ...state,
        hints: state.hints.filter(hint => hint.name !== action.hint.name)
      };
    case ADD_HOST:
      const newHost = document.createElement('div');
      newHost.classList.add('HintHost');
      document.documentElement.appendChild(newHost);
      return {
        ...state,
        hintHost: newHost
      };
    case START_HINTING:
      window.addEventListener('keydown', detectHintSequence, true);
      return {
        ...state,
        isHintingActive: true
      };
    case STOP_HINTING:
      window.removeEventListener('keydown', detectHintSequence, true);
      state.hints.forEach(hint => {
        hint.host.classList.remove('HintElem');
        hint.host.classList.remove('HintElemInactive');
      });
      state.hintHost.remove();
      return {
        ...state,
        hintHost: null,
        hints: [],
        isHintingActive: false
      };
    case RESET_HINTING:
      for (let hint of state.hints) {
        hint.hidden = false;
      }
      return {
        ...state,
        keySequence: [],
      };
    case PRESSED_KEY:
      const seq = action.time - state.lastKeyTime > 1000
        ? [action.key]
        : state.keySequence.concat(action.key);
      state.hints.forEach(hint => hintMatchesSequence(hint, seq)
        ? hint.hidden = false
        : hint.hidden = true)
      return {
        ...state,
        keySequence: seq,
        lastKeyTime: action.time,
      };
    case SHOW_HELPER:
      const h = state.helper === null
        ? Helper()
        : state.helper;
      if (!!state.helper) {
        state.helper.hidden = false;
      } else {
        document.documentElement.appendChild(h);
      }
      return {
        ...state,
        helper: h,
      };
    case HIDE_HELPER:
      state.helper.hidden = true;
      return {
        ...state,
      };
    default:
      return state;
  }
}
