import { createStore } from 'redux';
import { scrollReducer } from './reducers';

const store = createStore(scrollReducer);

export default store;
