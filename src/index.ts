import * as _ from 'lodash';
import { getHintNames } from './hint';
import { Direction, ScrollDirection } from './types';
import {
  newMouseCoords,
  newScrollTarget,
  addHint,
  addHost,
  startHinting,
  showHelper,
  hideHelper,
} from './actions';
import '../static/main.css';
import store from './store';
import Hint from './hint';

const UNSCROLLABELS = ['SPAN', 'CODE'];
// TODO: Don't allow deactivated input's or buttons
const SELECTABLES = ['button', 'input', 'summary', 'select', 'a'];
const SCROLL_AMOUNT = 400;

// Attach a hint box to the param element
const addHintToHost = (host: Element, name: string) => {
  const hint = new Hint(host as HTMLElement, name);
  store.dispatch(addHint(hint));
};

const isElemInViewport = (elem: Element): boolean => {
  const bounds = elem.getBoundingClientRect();
  const notHidden = elem.getAttribute('type') !== 'hidden'
    && window.getComputedStyle(elem, null).display !== 'none'
    && window.getComputedStyle(elem, null).visibility !== 'hidden'
    && (elem as any).offsetParent !== null;
  return notHidden
    && bounds.top >= 0
    && bounds.left >= 0
    && bounds.right <= (window.innerWidth || document.documentElement.clientWidth)
    && bounds.bottom <= (window.innerHeight || document.documentElement.clientHeight);
};

const getSelectableElems = (): Element[] => {
  return SELECTABLES
    .map(node => {
      const found: Element[] = [];
      const tags = document.getElementsByTagName(node);
      for (let tag of tags) {
        isElemInViewport(tag) && found.push(tag);
      }
      return found;
    })
    .reduce((acc, val) => acc.concat(val), []);
};

const getScrollOpts = (
  direction: string,
  scrollAmount: number
): ScrollToOptions => {
  const top = direction === "Up"
    ? -scrollAmount
    : direction === "Down"
      ? scrollAmount
      : 0;
  const left = direction === "Right"
    ? scrollAmount / 2
    : direction === "Left"
      ? -(scrollAmount / 2)
      : 0;
  return {
    top,
    left,
    behavior: 'smooth'
  };
};


const updateMouseCoords = (x: number, y: number): void => {
  store.dispatch(newMouseCoords(x, y));
};


/**
 * Returns true if the user is currently typing something.  Usually
 * typing takes place inside input elements or textareas.  However,
 * occasionally text is typed directly to other elements, in which case
 * these elements have the `contenteditable` attribute set to true.
 */
const isInputElemFocused = (): boolean => {
  const inputElems = ['INPUT', 'TEXTAREA'];
  const { activeElement } = document;
  const { nodeName } = activeElement;
  return inputElems.includes(nodeName)
    || activeElement.getAttribute('contenteditable') === "true";
};


const updateTarget = (
  x: number,
  y: number
): Element => {
  const target = document.elementFromPoint(x, y);
  !target.isEqualNode(store.getState().currentTarget)
    && store.dispatch(newScrollTarget(target))
  return target;
};


const isScrollable = (direction: ScrollDirection, element: Element): boolean => {
  switch (direction) {
    case "HORIZONTAL":
      return element.scrollWidth > element.clientWidth;
    case "VERTICAL":
      return element.scrollHeight > element.clientHeight;
    default:
      return false;
  }
};


/**
 * This is used to traverse the DOM tree, when the cursor is pointed at
 * an unscrollable element, which parent is the actual element we want
 * to scroll. This is the case e.g. with span an inline code elements.
 */
const getScrollTarget = (element: Element): Element => {
  if (UNSCROLLABELS.includes(element.nodeName) && !!element.parentElement) {
    return getScrollTarget(element.parentElement);
  } else {
    return element;
  }
};

/**
 * A couple things take place when hinting is executed.
 * 1. The hint host element is added to the page root
 * 2. All hintable elements are traversed, and those that are visible
 * get a hint attached to them
 * 3. We start listening for keypresses.
 */
const handleHinting = (event: KeyboardEvent) => {
  if (!event.shiftKey || isInputElemFocused()) return;
  store.dispatch(addHost());
  const elems = getSelectableElems().slice(0, 144);
  const hints = Array.from(getHintNames(elems.length));
  for (const [element, hint] of _.zip(elems, hints)) {
    addHintToHost(element, hint);
  }
  store.dispatch(startHinting());
};

const handleScrolling = (event: KeyboardEvent) => {
  const dir = Direction[event.keyCode];
  if (!event.shiftKey || !dir || isInputElemFocused()) return;
  const scrollDir = ["Up", "Down"].includes(dir)
    ? "VERTICAL"
    : "HORIZONTAL";
  const scrollOpts = getScrollOpts(dir, SCROLL_AMOUNT);
  const { x, y } = store.getState();
  const target = updateTarget(x, y);
  const firstScrollable = getScrollTarget(target);
  const scrollTarget = isScrollable(scrollDir, firstScrollable)
    ? firstScrollable
    : window;
  scrollTarget.scrollBy(scrollOpts);
};


/**
 * Scrolls to element currently under the pointer, or alternatively
 * scrolls the whole page if nothing can be scrolled under pointer.
 */
const handleKeypress = (event: KeyboardEvent): void => {
  if (store.getState().isHintingActive) return;
  switch (event.keyCode) {
    case 16:
      store.dispatch(showHelper());
      break;
    case 70:
      handleHinting(event);
      break;
    default:
      handleScrolling(event);
      break;
  }
};

const handleKeyup = (event: KeyboardEvent): void => {
  switch (event.keyCode) {
    case 16:
      store.dispatch(hideHelper());
    default:
  }
}

window.addEventListener("keydown", handleKeypress);
window.addEventListener("keyup", handleKeyup);
window.addEventListener("mousemove", ({x, y}: MouseEvent) => updateMouseCoords(x, y));
