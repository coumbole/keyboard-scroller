## Keyboard scroller

This Firefox add-on makes it possible to scroll web pages with a keyboard.


#### Usage

To scroll, simply hold Shift key and use WASD keys to scroll the page. The cursor can be used to target which element should be scrolled.