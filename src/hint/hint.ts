import {
  addHint
} from '../actions';

// Adapted from Tridactyl extension's hinting implementation
// TODO: Add onSelect feature
export default class Hint {
  public readonly container = document.createElement('span');
  public readonly bounds: ClientRect = null;
  public readonly onSelect: () => void = () => {
    switch (this.host.tagName.toLowerCase()) {
      case "a":
        this.handleAnchor(this.host as HTMLAnchorElement);
        break;
      case "input":
        this.handleInput(this.host as HTMLInputElement);
        break;
      case "button":
        this.handleGeneric(this.host);
        break;
      case "summary":
        this.handleGeneric(this.host);
        break;
      default:
        this.handleGeneric(this.host);
        break;
    }
  }

  private x = 0;
  private y = 0;
  public width = 0;
  public height = 0;

  constructor(
    public readonly host: HTMLElement,
    public name: string
  ) {
    const padding = 4;

    const offset = 20;

    const rects = host.getClientRects();
    let rect = rects[0];

    for (let r of rects) {
      if (r.bottom > 0 && r.right > 0)
        rect = r;
        break;
    }

    this.bounds = {
      top: rect.top,
      bottom: rect.bottom,
      left: rect.left,
      right: rect.right,
      width: rect.width,
      height: rect.height
    };

    this.container.textContent = name;
    this.container.className = "HintBox";

    this.host.classList.add('HintElem');

    const top = rect.top > 0 ? this.bounds.top : padding;
    const left = rect.left > 0 ? this.bounds.left : padding;

    this.x = window.scrollX + left;
    this.y = window.scrollY + top;

    this.updatePosition();
  }

  set hidden(hide: boolean) {
    this.container.hidden = hide;
    if (hide) {
      this.host.classList.remove('HintElem');
      this.host.classList.add('HintElemInactive');
      this.container.classList.add('HintBoxInactive');
    } else {
      this.host.classList.add('HintElem');
      this.host.classList.remove('HintElemInactive');
      this.container.classList.remove('HintBoxInactive');
    }
  }

  private updatePosition() {
    this.container.style.cssText = `
    top: ${this.y}px !important;
    left: ${this.x}px !important;
    `;
  }

  private handleAnchor(link: HTMLAnchorElement) {
    const isSameOrigin = link.hostname === window.location.hostname;
    isSameOrigin
      ? link.click()
      : window.open(link.href, '_blank');
  }

  private handleInput(input: HTMLInputElement) {
    input.focus();
  }

  private handleGeneric(button: HTMLElement) {
    button.click();
  }
}
