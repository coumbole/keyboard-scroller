import { keymap } from '../config';
import { KeymapEntry, Tip } from '../types';


const tips: Tip[] = [
  {
    title: "Mouse",
    description: "Point the element you wish to scroll",
  },
  {
    title: "Canceling",
    description: "Press Esc or any other key not shown in the hints to cancel hinting.",
  },
  {
    title: "Hinting",
    description: "Press the key combination shown in a hint to select it",
  },
];

const createRow = (entry: Tip): HTMLTableRowElement => {
  const row = document.createElement('tr');
  row.classList.add('HelperEntry');

  const key = document.createElement('td')
  key.textContent = `${entry.title}`;
  row.appendChild(key);

  const desc = document.createElement('td')
  desc.textContent = `${entry.description}`;
  row.appendChild(desc);

  return row;
}

const Helper = (): HTMLElement => {
  const container = document.createElement('div');
  container.classList.add('HelperContainer');

  const table: HTMLTableElement = document.createElement('table');
  table.classList.add('HelperContent');

  const caption = document.createElement('caption');
  caption.textContent = `Help`;
  table.appendChild(caption);

  const tBody = document.createElement('tbody');

  keymap
    .map(entry => entry as Tip)
    .concat(tips)
    .map(createRow)
    .forEach(row => tBody.appendChild(row));

  table.appendChild(tBody);
  container.appendChild(table);

  return container;
};

export default Helper;
