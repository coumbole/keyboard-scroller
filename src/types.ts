import Hint from './hint';

export enum Direction {
  Up = 87,
  Down = 83,
  Left = 65,
  Right = 68 
}

export type ScrollDirection = "VERTICAL" | "HORIZONTAL";

export interface State {
  currentTarget: Element | null;
  x: number;
  y: number;
  hints: Hint[];
  hintHost: Element | null;
  isHintingActive: boolean;
  keySequence: string[];
  lastKeyTime: number;
  timeout: any;
  helper: HTMLElement | null;
}

export type ScrollAction = NewScrollTarget | NewMouseCoords;
export type HintAction = AddHint
  | RemoveHint
  | AddHost
  | StartHinting
  | StopHinting
  | ResetHinting
  | PressedKey
  | ShowHelper
  | HideHelper;
export type Action = ScrollAction | HintAction;

export const NEW_SCROLL_TARGET = "NEW_SCROLL_TARGET";

interface NewScrollTarget {
  type: typeof NEW_SCROLL_TARGET,
  target: Element
}

export const NEW_MOUSE_COORDS = "NEW_MOUSE_COORDS";

interface NewMouseCoords {
  type: typeof NEW_MOUSE_COORDS,
  x: number,
  y: number
}

export const ADD_HINT = "ADD_HINT";

interface AddHint {
  type: typeof ADD_HINT,
  hint: Hint,
}

export const REMOVE_HINT = "REMOVE_HINT";

interface RemoveHint {
  type: typeof REMOVE_HINT,
  hint: Hint,
}

export const ADD_HOST = "ADD_HOST";

interface AddHost {
  type: typeof ADD_HOST
}

export const START_HINTING = "START_HINTING";

interface StartHinting {
  type: typeof START_HINTING
}

export const STOP_HINTING = "STOP_HINTING";

interface StopHinting {
  type: typeof STOP_HINTING
}

export const RESET_HINTING = "RESET_HINTING";

interface ResetHinting {
  type: typeof RESET_HINTING
}

export const PRESSED_KEY = "PRESSED_KEY";

interface PressedKey {
  type: typeof PRESSED_KEY,
  key: string,
  time: number
}

export const SHOW_HELPER = "SHOW_HELPER";

interface ShowHelper {
  type: typeof SHOW_HELPER
}

export const HIDE_HELPER = "HIDE_HELPER";

interface HideHelper {
  type: typeof HIDE_HELPER
}

export interface Tip {
  title: string;
  description: string;
}

export interface KeymapEntry extends Tip {
  keyCode: number;
}
