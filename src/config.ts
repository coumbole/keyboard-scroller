import { KeymapEntry } from './types';

export const keymap: KeymapEntry[] = [
  {
    keyCode: 16,
    title: "Shift",
    description: "Activate keyboard commands"
  },
  {
    keyCode: 87,
    title: "W",
    description: "Scroll up"
  },
  {
    keyCode: 65,
    title: "A",
    description: "Scroll left",
  },
  {
    keyCode: 83,
    title: "S",
    description: "Scroll down",
  },
  {
    keyCode: 68,
    title: "D",
    description: "Scroll right",
  },
  {
    keyCode: 70,
    title: "F",
    description: "Activate hinting",
  },
];
