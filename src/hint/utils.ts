import Hint from './hint';
import store from '../store';
import { stopHinting, resetHinting, keyPressed } from '../actions';

let timeout: any;

/**
 * Generates [n] hint names, 0 < n < 132
 *
 * @return an iterator of hint name strings
 */
export function* getHintNames(n: number): IterableIterator<string> {
  const hintchars = 'qwerasdfzxcv';
  let hintCount = n < 13
    ? 0
    : 12;

  let neededHints = n > hintchars.length ** 2
    ? hintchars.length ** 2
    : n;
  neededHints += hintCount;
  while (hintCount < neededHints) {
    const currentHintCharCount = hintCount < 12
      ? 1
      : 2;
    let hint = '';
    // i represents each char in the final hint
    for (let i = 0; i < currentHintCharCount; i++) {

      // First char (if 2 char hint)
      const prefix = Math.floor(hintCount / hintchars.length) - 1;

      // Last digit (or the only one if 1 char hint)
      const index = hintCount % hintchars.length;

      hint += i < currentHintCharCount - 1
        ? hintchars[prefix]
        : hintchars[index];
    }
    yield hint;
    hintCount += 1;
  }
}

export const hintMatchesSequence = (hint: Hint, sequence: string[]): boolean => {
  return hint.name.toLowerCase().startsWith(sequence.join(""));
};

export const detectHintSequence = (event: KeyboardEvent) => {
  event.preventDefault();
  const charlist = 'qwerasdfzxcv';
  const key = event.key.toLowerCase();
  if (charlist.indexOf(key) < 0) {
    store.dispatch(stopHinting());
    return;
  };

  timeout = setTimeout(() => store.dispatch(resetHinting()), 3000);
  store.dispatch(keyPressed(key))

  const { hints, keySequence } = store.getState();
  const visibleHints = hints.filter(h => !h.hidden);
  const selectedHint = keySequence.join("");
  const matchingHints = hints.filter(h => hintMatchesSequence(h, keySequence));

  if (matchingHints.length === 0) {
    store.dispatch(stopHinting());
    clearTimeout(timeout);
    // TODO: Do we need to do something else?
    return;
  } else if (matchingHints.length === 1) {
    matchingHints[0].onSelect();
    store.dispatch(stopHinting());
    clearTimeout(timeout);
    return;
  }
};
